<?php
/**
 * @file
 * Administrative forms and callbacks.
 *
 */

/**
 * Bookings overview page.
 */
function publicbookings_overview_page() {
  include_once('publicbookings.views_default.inc');
  $text = '<h3>'.t('Awaiting approval').'</h3>';
  $view = publicbookings_view_schedules_admin();
  //echo '<pre>';
  //print_r($view);
  //echo '</pre>';
  $text .= $view->preview(NULL, array(1)); // first argument is the status code
  $text .= '<h3>'.t('Finalized').'</h3>';
  $view = publicbookings_view_schedules_admin();
  $text .= $view->preview(NULL, array(2));
  return $text;
}

function publicbookings_booking_list($where = '', $element = 0) {
  include_once('publicbookings.views_default.inc');
  $view = publicbookings_view_schedules_admin();
  return $view->render();
}

/**
 * Bookings add page.
 */
function publicbookings_booking_add() {
  return drupal_get_form('publicbookings_booking_add_form');
}

/**
 * Bookings edit page.
 *
 * @param array $booking
 */
function publicbookings_booking_edit($booking) {
  return drupal_get_form('publicbookings_booking_edit_form', $booking);
}

/**
 * Bookings form.
 *
 * @param array $form_state
 * @param array $booking
 * @return array
 */
function publicbookings_booking_form(&$form_state, $booking = array()) {
  include_once('./'. drupal_get_path('module', 'bookingsapi') .'/bookingsapi.forms.inc');
  $form = bookingsapi_booking_form($booking);
  if (empty($booking['start']) && empty($booking['end'])) {
    $default_date = date_format(date_now(), DATE_DATETIME);
    $form['basic']['start']['#default_value'] = $default_date;
    $form['basic']['end']['#default_value'] = $default_date;
  }

  if ($booking['resource_id'] > 0) {
    // if a resource ID is given, don't let the user change it
    $form['basic']['resource_id']['#value'] = $booking['resource_id'];
    $form['basic']['resource_id']['#disabled'] = TRUE;
  }

  drupal_add_js(drupal_get_path('module', 'publicbookings') . '/ajax.js');
  // AJAX availability checker
  $form['basic']['ajax_display'] = array(
    '#value' => '<div id="ajax-availability">Enable JavaScript.</div>',
  );

  $client = publicbookings_client_load_from_record_id($booking['record_id']);
  // If client information exists, disable all the fields except the comment box, and preserve all the values.
  if ($client) {
    include_once('./'. drupal_get_path('module', 'publicbookings') .'/publicbookings.public.inc');
    $clientform = publicbookings_clientinfo_form($client);
    $clientform['client']['#title'] = t('Client details');
    $clientform['client']['first_name']['#disabled'] = TRUE;
    $clientform['client']['first_name']['#value'] = $clientform['client']['first_name']['#default_value'];
    $clientform['client']['last_name']['#disabled'] = TRUE;
    $clientform['client']['last_name']['#value'] = $clientform['client']['last_name']['#default_value'];
    $clientform['client']['phone']['#disabled'] = TRUE;
    $clientform['client']['phone']['#value'] = $clientform['client']['phone']['#default_value'];
    $clientform['client']['email']['#disabled'] = TRUE;
    $clientform['client']['email']['#value'] = $clientform['client']['email']['#default_value'];
    $form = array_merge($form, $clientform);
    $form['basic']['status']['#description'] .= '<br />' . t('This booking was made by a public client.
      Changing the status will dispatch a notification email to the client.');
  }


  $form['#base'] = 'publicbookings_booking_form';
  $form['#submit'] = array('publicbookings_booking_form_submit');
  $form['#validate'][] = 'publicbookings_booking_form_validate';
  return $form;
}

/**
 * Bookings form validation. Checks for scheduling conflicts, and that the RRULE has an UNTIL.
 *
 * @param array $form
 * @param array $form_state
 */
function publicbookings_booking_form_validate($form, &$form_state) {
  // If we're deleting, conflicts and validity don't matter.
  if ($form_state['values']['op'] == t('Delete')) return;
  
  if ( $form_state['values']['INTERVAL'] != 0 ) {
    $form_state['values']['rrule_until'] = $form_state['values']['UNTIL']['datetime'];
    if (empty($form_state['values']['rrule_until'])) {
      form_set_error('rrule', t('Recurrences must have an end date.'));
    }
    else {
      $form_state['values']['rrule'] = date_api_ical_build_rrule($form_state['values']);
    }
  }
  publicbookings_conflict_check($form_state['values']);
  if ($form_state['values']['count_certain_conflicts'] > 0) {
    form_set_error('basic', $form_state['values']['html_certain_conflicts']);
  }
}

function publicbookings_booking_form_submit($form, &$form_state) {
  // If we're deleting, redirect to the deletion page immediately.
  if ($form_state['values']['op'] == t('Delete')) {
    $form_state['redirect'] = array('admin/content/publicbookings/booking/'.$form_state['values']['record_id'].'/delete');
    return;
  }

  $save_result = publicbookings_booking_save($form_state['values']);
  $t_args = array('%name' => $form_state['values']['name']);
  // no message for BOOKINGSAPI_TIME_CONFLICT, as we should have prevented it before we got here
  switch ($save_result) {
    case SAVED_NEW:
      $op = 'created'; break;
    case SAVED_UPDATED:
      $op = 'updated'; break;
    default:
      $op = 'failed';
  }
  drupal_set_message(t("The booking %name has been $op.", $t_args));
  $form_state['redirect'] = 'admin/content/publicbookings/booking/list';
}

function publicbookings_resource_overview() {
  return publicbookings_resource_list();
}

/**
 * Resource listing page.
 */
function publicbookings_resource_list() {
  include_once('publicbookings.views_default.inc');
  $view = publicbookings_view_resources_admin();
  return $view->render();
}

/**
 * Booking delete confirmation page.
 * @param array $booking
 */
function publicbookings_booking_delete($booking) {
  return drupal_get_form('publicbookings_booking_delete_confirm', $booking);
}

/**
 * Booking delete confirmation form.
 */
function publicbookings_booking_delete_confirm(&$form_state, $booking) {
  $form['#item'] = $booking;
  $desc = t('%name, and all of its instances, will be deleted.', array('%name'=>$booking['name']));
  return confirm_form($form, t('Are you sure you want to delete this booking?'),
    'admin/content/publicbookings',$desc,t('Delete'));
}

/**
 * Booking delete.
 */
function publicbookings_booking_delete_confirm_submit($form, &$form_state) {
  bookingsapi_record_delete($form['#item']);
  $t_args = array('%name' => $form['#item']['name']);
  drupal_set_message(t('The booking %name has been deleted.', $t_args));
  $form_state['redirect'] = 'admin/content/publicbookings';
}

/**
 * Resource adding page.
 */
function publicbookings_resource_add() {
  return drupal_get_form('publicbookings_resource_add_form');
}

/**
 * Resource editing page.
 */
function publicbookings_resource_edit($resource) {
  return drupal_get_form('publicbookings_resource_edit_form', $resource);
}

/**
 * Resource delete confirmation page.
 * @param array $resource
 */
function publicbookings_resource_delete($resource) {
  return drupal_get_form('publicbookings_resource_delete_confirm', $resource);
}

/**
 * Resource delete confirmation form.
 */
function publicbookings_resource_delete_confirm(&$form_state, $resource) {
  $form['#item'] = $resource;
  $desc = t('All booking and availability records associated with %name will also be deleted.', array('%name'=>$resource['name']));
  return confirm_form($form, t('Are you sure you want to delete this resource?'),
    'admin/content/publicbookings/resource',$desc,t('Delete'));
}

/**
 * Resource delete.
 */
function publicbookings_resource_delete_confirm_submit($form, &$form_state) {
  bookingsapi_resource_delete($form['#item']);
  $t_args = array('%name' => $form['#item']['name']);
  drupal_set_message(t('The bookable resource %name has been deleted.', $t_args));
  $form_state['redirect'] = 'admin/content/publicbookings/resource';
}

/**
 * Resources form.
 */
function publicbookings_resource_form(&$form_state, $resource = array()) {
  include_once('./'. drupal_get_path('module', 'bookingsapi') .'/bookingsapi.forms.inc');
  $form = bookingsapi_resource_form($resource);
  if (!empty($resource)) $form['delete']['#submit'] = array('publicbookings_resource_delete');
  $form['#submit'] = array('publicbookings_resource_form_submit');
  $form['#validate'] = array('publicbookings_resource_form_validate');
  return $form;
}

/**
 * Resource form validation.
 */
function publicbookings_resource_form_validate($form, &$form_state) {

}

/**
 * Resource form submit.
 */
function publicbookings_resource_form_submit($form, &$form_state) {
  $save_result = bookingsapi_resource_save($form_state['values']);
  $t_args = array('%name' => $form_state['values']['name']);
  switch($save_result) {
    case SAVED_NEW:
      $op = 'created'; break;
    case SAVED_UPDATED:
      $op = 'updated'; break;
    default:
      $op = 'failed';
  }
  drupal_set_message(t("The bookable resource %name has been $op.", $t_args));
  if($op=='failed') return false;
  $form_state['redirect'] = 'admin/content/publicbookings/resource';
}


/**
 * Availability list page.
 */
function publicbookings_availability_list($resource) {
  include_once('./'. drupal_get_path('module', 'bookingsapi') .'/bookingsapi.forms.inc');
  $avail_code = bookingsapi_numeric_record_types($resource['default_availability']);
  $text .= '<h2>'.$resource['name'].'</h2>';
  $avail = bookingsapi_availability_types();
  $text .= '<strong>'.t('Default availability:').'</strong> ' . $avail[$avail_code];

  include_once('publicbookings.views_default.inc');
  $view = publicbookings_view_availability_admin();
  $text .= $view->preview(NULL, array($resource['resource_id'])); // first argument is the resource ID
  return $text;
}

/**
 * Availability add page.
 */
function publicbookings_availability_add($resource) {
  include_once('./'. drupal_get_path('module', 'bookingsapi') .'/bookingsapi.forms.inc');
  $fakeavail = array('resource_id'=>$resource['resource_id']);
  return drupal_get_form('publicbookings_availability_add_form', $fakeavail);
}

function publicbookings_availability_edit($avail) {
  return drupal_get_form('publicbookings_availability_edit_form', $avail);
}

/**
 * Availability delete confirmation page.
 * @param array availability
 */
function publicbookings_availability_delete($avail) {
  return drupal_get_form('publicbookings_availability_delete_confirm', $avail);
}

/**
 * Availability delete confirmation form.
 */
function publicbookings_availability_delete_confirm(&$form_state, $avail) {
  $form['#item'] = $avail;
  $desc = t('%name, and all of its instances, will be deleted.', array('%name'=>$avail['name']));
  return confirm_form($form, t('Are you sure you want to delete this availability?'),
    'admin/content/publicbookings',$desc,t('Delete'));
}

/**
 * Availability delete.
 */
function publicbookings_availability_delete_confirm_submit($form, &$form_state) {
  bookingsapi_availability_delete($form['#item']);
  $t_args = array('%name' => $form['#item']['name']);
  drupal_set_message(t('The availability %name has been deleted.', $t_args));
  $form_state['redirect'] = 'admin/content/publicbookings';
}

function publicbookings_availability_form(&$form_state, $avail = array()) {
  include_once('./'. drupal_get_path('module', 'bookingsapi') .'/bookingsapi.forms.inc');
  $form = bookingsapi_availability_form($avail);
  if (empty($avail['start']) && empty($avail['end'])) {
    $default_date = date_format(date_now(), DATE_DATETIME);
    $form['basic']['start']['#default_value'] = $default_date;
    $form['basic']['end']['#default_value'] = $default_date;
  }
  if ($avail['resource_id'] > 0) {
    // if a resource ID is given, don't let the user change it
    $form['basic']['resource_id']['#value'] = $avail['resource_id'];
    $form['basic']['resource_id']['#disabled'] = TRUE;
    $res = bookingsapi_resource_load($avail['resource_id']);
    if($res['disabled']) {
      // disabled resources don't appear in the dropdown menu be default
      // so if disabled, make the menu show all
      $form['basic']['resource_id']['#bookingsapi_showall'] = TRUE;
    }
  }
  $form['#base'] = 'publicbookings_availability_form';
  $form['#submit'] = array('publicbookings_availability_form_submit');
  $form['#validate'][] = 'publicbookingsavailability_form_validate';
  return $form;
}

// temporarily identical to booking equivalent
function publicbookings_availability_form_validate($form, &$form_state) {
  // If we're deleting, conflicts and validity don't matter.
  if ($form_state['values']['op'] == t('Delete')) return;

  if (!empty($form_state['values']['ical_rrule'])) {
    $form_state['values']['ical_rrule_until'] = bookingsapi_rrule_until_as_dt($form_state['values']['ical_rrule']);
    if(empty($form_state['values']['ical_rrule_until'])) {
      form_set_error('ical_rrule', t('Recurrences must have an end date.'));
    }
  }
  if ($form_state['values']['type'] == BOOKINGSAPI_UNA_R) {
    $fields = '{bookings_records}.name, {bookings_schedules}.type, {bookings_schedules}.`start`, {bookings_schedules}.`end`';
    $conflicts = bookingsapi_conflict_check($form_state['values'], $fields, 10);
    if (count($conflicts) > 0) {
      // TODO: create theme function
      // get resource name
      $errormsg = '<strong>'.t('Conflicts found').'</strong>'.
          '<p>'.t('The following conflicts with your unavailability:').'</p><ul>';
      foreach ($conflicts as $c) {
        $errormsg .= '<li>'.publicbookings_types_translate($c['type']).': '.$c['name'].', from '.$c['start'].' to '.$c['end'].'</li>';
      }
      $errormsg .= '</ul>';
      form_set_error('basic', $errormsg);
    }
  }
}

// temporarily identical to booking equivalent
function publicbookings_availability_form_submit($form, &$form_state) {
  // If we're deleting, redirect to the deletion page immediately.
  if ($form_state['values']['op'] == t('Delete')) {
    $form_state['redirect'] = array('admin/content/publicbookings/availability/'.$form_state['values']['record_id'].'/delete');
    return;
  }

  $save_result = bookingsapi_availability_save($form_state['values']);
  $t_args = array('%name' => $form_state['values']['name']);
  // no message for BOOKINGSAPI_TIME_CONFLICT, as we should have prevented it before we got here
  switch ($save_result) {
    case SAVED_NEW:
      $op = 'created'; break;
    case SAVED_UPDATED:
      $op = 'updated'; break;
    default:
      $op = 'failed';
  }
  drupal_set_message(t("The availability %name has been $op.", $t_args));
  $form_state['redirect'] = 'admin/content/publicbookings/resource/'.$form_state['values']['resource_id'].'/availability';
}

/**
 * Settings page.
 */
function publicbookings_settings() {
  return drupal_get_form('publicbookings_settings_form');
}

/**
 * Settings form.
 */
function publicbookings_settings_form() {
  $form = array();
  $form['basic'] = array(
    '#type' => 'fieldset',
    '#title' => t('Basic settings'),
  );
  $form['basic']['publicbookings_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable the public bookings form'),
    '#default_value' => variable_get('publicbookings_enabled', 0),
    '#description' => t('If unchecked, all public request functionality will be disabled.
      The administrative interface will not be affected.'),
  );

  $form['basic']['bookingsapi_time_granularity'] = array(
    '#type' => 'textfield',
    '#title' => t('Time granularity'),
    '#default_value' => variable_get('bookingsapi_time_granularity', 10),
    '#description' => t('The increments for datetime selectors, in minutes.'),
  );

  $form['basic']['bookingsapi_using_sql_ts_triggers'] = array(
    '#type' => 'checkbox',
    '#title' => t('Tables have triggers defined for updating the \'created\' and \'modified\' timestamps.'),
    '#default_value' => variable_get('bookingsapi_using_sql_ts_triggers', 0),
    '#description' => t('If checked, the updating of these fields will be delegated entirely to the MySQL triggers.'),
  );

  $form['#validate'] = array(
    'publicbookings_settings_validate' => array()
  );
  return system_settings_form($form);
}

/**
 * Settings form validation.
 */
function publicbookings_settings_validate($form, &$form_state) {
  if(!is_numeric($form_state['values']['bookingsapi_time_granularity'])) {
    form_set_error('bookingsapi_time_granularity', t('Must be an integer.'));
  }
}

/**
 * Settings form submit.
 */
function publicbookings_settings_submit($form, &$form_state) {
  variable_set('publicbookings_enabled', (bool) $form_state['values']['publicbookings_enabled']);
  variable_set('bookingsapi_time_granularity', (int) $form_state['values']['bookingsapi_time_granularity']);
  variable_set('bookingsapi_using_sql_ts_triggers', (bool) $form_state['values']['bookingsapi_using_sql_ts_triggers']);
}

