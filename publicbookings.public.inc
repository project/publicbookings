<?php
/**
 * @file
 * Public Bookings - Public interface
 */

function publicbookings_clientinfo_form($values) {
  $form = array();
  $form['client'] = array(
    '#title' => t('Your details'),
    '#type' => 'fieldset',
  );
  $form['client']['client_id'] = array(
    '#type' => 'value',
    '#value' => $values['client_id'],
  );
  $form['client']['first_name'] = array(
    '#title' => t('First name'),
    '#type' => 'textfield',
    '#default_value' => $values['first_name'],
    '#required' => TRUE,
  );
  $form['client']['last_name'] = array(
    '#title' => t('Last name'),
    '#type' => 'textfield',
    '#default_value' => $values['last_name'],
  );
  $form['client']['phone'] = array(
    '#title' => t('Phone number'),
    '#type' => 'textfield',
    '#description' => t('10 digits.'),
    '#default_value' => $values['phone'],
  );
  $form['client']['email'] = array(
    '#title' => t('Email address'),
    '#type' => 'textfield',
    '#description' => t('Your booking will be confirmed by email.'),
    '#default_value' => $values['email'],
    '#required' => TRUE,
  );
  $form['client']['comments'] = array(
    '#title' => t('Special instructions or comments'),
    '#type' => 'textarea',
    '#description' => t('If this booking has any special requirements, specify them here.'),
    '#default_value' => $values['comments'],
  );
  if(isset($values['created'])) {
    $form['client']['created'] = array(
      '#type' => 'item',
      '#title' => t('Created'),
      '#value' => $values['created'],
    );
    $form['client']['modified'] = array(
      '#type' => 'item',
      '#title' => t('Modified'),
      '#value' => $values['modified'],
    );
  }
  $form['client']['#validate'] = array('publicbookings_clientinfo_form_validate');

  return $form;
}

function publicbookings_clientinfo_form_validate($form, &$form_state) {
  // remove all non-digits
  $form_state['values']['phone'] = preg_replace('/\D/','',$form_state['values']['phone']);
}

function publicbookings_passphrase_form() {
  $form = array();
  $form['client'] = array(
    '#title' => t('Update request'),
    '#type' => 'fieldset',
  );
  $form['client']['passphrase'] = array(
    '#title' => t('Passphrase'),
    '#type' => 'textfield',
    '#description' => t('Your booking request\'s passphrase is found in your initial confirmation email.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Proceed'),
  );
  $form['#validate'] = array('publicbookings_passphrase_validate');
  $form['#submit'] = array('publicbookings_passphrase_submit');
  return $form;
}

function publicbookings_request_form(&$form_state, $values) {
  include_once('./'. drupal_get_path('module', 'bookingsapi') .'/bookingsapi.forms.inc');
  $form = array_merge(bookingsapi_booking_form($values), publicbookings_clientinfo_form($values));
  $form['basic']['#title'] = t('Booking request');
  // until email confirmation, the status is UNCONFIRMED. afterwards, it becomes PENDING.
  $form['basic']['status'] = array(
    '#type' => 'value',
    '#value' => BOOKINGSAPI_STATUS_UNCONFIRMED,
  );
  if (empty($booking['start']) && empty($booking['end'])) {
    $default_date = date_format(date_now(), DATE_DATETIME);
    $form['basic']['start']['#default_value'] = $default_date;
    $form['basic']['end']['#default_value'] = $default_date;
  }

  // repeat functionality is probably unnecessary for public use
  unset($form['basic']['rrule']);

  if (!empty($values)) {
    // clients don't have option to delete
    unset($form['delete']);

    // for consistency, do not allow the changing of the requested resource, or the client email
    $form['basic']['resource_id']['#disabled'] = TRUE;
    $form['basic']['resource_id']['#value'] = $form['basic']['resource_id']['#default_value'];
    $form['client']['email']['#disabled'] = TRUE;
    $form['client']['email']['#value'] = $form['basic']['email']['#default_value'];
  }

  // implement later
  unset($form['basic']['priority']);

  // AJAX availability checker
  drupal_add_js(drupal_get_path('module', 'publicbookings') . '/ajax.js');
  $form['basic']['ajax_display'] = array(
    '#value' => '<div id="ajax-availability">Enable JavaScript.</div>',
  );

  $form['#validate'][] = 'publicbookings_request_form_validate';
  $form['#submit'] = array('publicbookings_request_form_submit');

  $form['submit'] = array(
    '#type' => 'submit',
    '#weight' => 8,
  );
  if(empty($values['record_id'])) {
    $form['submit']['#value'] = t('Submit request');
  } else {
    $form['submit']['#value'] = t('Update request');
    $form['cancel'] = array(
      '#type' => 'submit',
      '#weight' => 9,
      '#value' => t('Cancel request'),
    );
  }
  return $form;
}

function publicbookings_request_form_validate($form, &$form_state) {
  publicbookings_conflict_check($form_state['values']);
  if($form_state['values']['count_certain_conflicts'] > 0) {
    form_set_error('basic', $form_state['values']['html_certain_conflicts']);
  }
}

function publicbookings_request_form_submit($form, &$form_state) {
  $save_result = publicbookings_booking_save($form_state['values']);
  $t_args = array('%name' => $form_state['values']['name']);
  $blurb = '';
  // no message for BOOKINGSAPI_TIME_CONFLICT, as we should have prevented it before we got here
  switch ($save_result) {
    case SAVED_NEW:
      $op = t('submitted');
      $blurb .= '<p>'.t('A confirmation link has been sent to your email address.
            Check your email and visit the link to proceed.').'</p>';
      break;
    case SAVED_UPDATED:
      $op = t('updated');
      sess_write('publicbookings_authed_req_id', NULL);
      break;
  }
  $blurb = '<p>'.t("Your booking request %name has been $op.", $t_args).'</p>' . $blurb;
  $form_state['redirect'] = 'publicbookings/overview';
  drupal_set_message($blurb);
}

function publicbookings_public_overview_page() {
  include_once('publicbookings.views_default.inc');

  $text = '<h3>'.t('Currently occupied').'</h3>';
  $view = publicbookings_view_currently_happening();
  $text .= $view->render();

  $text .= '<h3>'.t('Upcoming').'</h3>';
  $view = publicbookings_view_upcoming();
  $text .= $view->render();

  return $text;
}

function publicbookings_public_request_add() {
  if(variable_get('publicbookings_enabled', 0) == 0) {
    return publicbookings_public_disabled_message();
  }
  return drupal_get_form('publicbookings_request_form', array());
}

function publicbookings_public_passphrase() {
  if (variable_get('publicbookings_enabled', 0) == 0) {
    return publicbookings_public_disabled_message();
  }

  $blurb = '<p>'.t('To update or cancel your booking request, enter the unique passphrase associated with your booking.').'</p>'.
           '<p>'.t('If you have lost your passphrase, or if your booking status has already been set to finalized, contact
           an administrator to make changes.').'</p>';
  return $blurb . drupal_get_form('publicbookings_passphrase_form');
}

function publicbookings_passphrase_validate($form, &$form_state) {
  $sql = "SELECT * FROM {publicbookings_clients} WHERE passphrase='%s'";
  $q = db_query($sql, $form_state['values']['passphrase']);
  $client = db_fetch_array($q);
  if ($client == FALSE) {
    form_set_error('passphrase', t('Invalid passphrase.'));
  } else {
    $form_state['client'] = $client;
  }
}

function publicbookings_confirm($passphrase) {
  if (variable_get('publicbookings_enabled', 0) == 0) {
    return publicbookings_public_disabled_message();
  }

  $sql = "SELECT pbc.email, bres.name AS resourceName, brec.start, brec.end, brec.name AS eventName, pbc.record_id, brec.resource_id " .
         "FROM {publicbookings_clients}   AS pbc " .
         "INNER JOIN {bookings_records}   AS brec ON pbc.record_id = brec.record_id ".
         "INNER JOIN {bookings_resources} AS bres ON brec.resource_id = bres.resource_id ".
         "WHERE pbc.passphrase = '%s'";
  $q = db_query($sql, $passphrase);
  $client = db_fetch_array($q);
  if ($client == FALSE) {
    return '<p>'.t('Invalid passphrase. Verify the link in your confirmation email.').'</p>';
  } else {
    bookingsapi_record_status_change($client, BOOKINGSAPI_STATUS_PENDING);
   $values = array(
      'name'=>$client['eventName'],
      'resource_name'=>$client['resourceName'],
      'start'=>$client['start'],
      'end'=>$client['end'],
   );
    drupal_mail('publicbookings', 'request_pending', $client['email'], language_default(), $values, NULL, TRUE);
    return '<p>'.t('Your email has been verified. Your booking request is now pending review by a booking manager.
            You will be informed of any status changes by email').'</p>';
  }
}

function publicbookings_passphrase_submit($form, &$form_state) {
  session_save_session(TRUE);
  sess_write('publicbookings_authed_req_id', $form_state['client']['client_id']);
  $form_state['redirect'] = 'publicbookings/request/'.$form_state['client']['client_id'].'/update';
}

function publicbookings_public_request_update($client) {
  if(sess_read('publicbookings_authed_req_id') != $client['client_id']) {
    return drupal_access_denied();
  }
  $booking = bookingsapi_booking_load($client['record_id']);
  $values = array_merge($booking, $client);
  return drupal_get_form('publicbookings_request_form', $values);
}

function publicbookings_public_schedules() {
  include_once('publicbookings.views_default.inc');
  $view = publicbookings_view_schedules();
  return $view->render();
}

function publicbookings_public_disabled_message() {
  $blurb = t('The public booking system is currently disabled. Try again later.');
  $blurb = '<p>'.$blurb.'</p>';
  return $blurb;
}