<?php
/**
 * @file
 * Default and internal views.
 *
 */

/**
 * Implementation of hook_views_default_views().
 *
 */
function publicbookings_views_default_views() {
  $view = publicbookings_view_schedules();
  $views[$view->name] = $view;
  
  $view = publicbookings_view_schedules_admin();
  $views[$view->name] = $view;
  
  $view = publicbookings_view_currently_happening();
  $views[$view->name] = $view;
  
  $view = publicbookings_view_current();
  $views[$view->name] = $view;
  
  $view = publicbookings_view_availability_admin();
  $views[$view->name] = $view;
  
  $view = publicbookings_view_resources_admin();
  $views[$view->name] = $view;
  
  $view = publicbookings_view_upcoming();
  $views[$view->name] = $view;
  
  return $views;
}

function publicbookings_default_view_object() {
  include_once('./'. drupal_get_path('module', 'views') .'/includes/view.inc');
  $view = new view;
  $view->description = '';
  $view->tag = 'Public Bookings';
  $view->view_php = '';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE;
  return $view;
}

function publicbookings_view_schedules() {

  $view = publicbookings_default_view_object();
  $view->name = 'publicbookings_view_schedules';
  $view->base_table = 'bookings_schedules';
  
  $view->fields = array(
    array(
      'tablename' => 'bookings_record',
      'field' => 'name',
      'handler' => 'views_handler_filter_string',
      'label' => 'Name',
      
    ),
    array(
      'tablename' => 'bookings_record',
      'field' => 'start',
      'handler' => 'views_handler_field_date',
      'label' => 'Start',
      
    ),
    array(
      'tablename' => 'bookings_record',
      'field' => 'end',
      'handler' => 'views_handler_field_date',
      'label' => 'End',
      
    ),
    array(
      'tablename' => 'bookings_record',
      'field' => 'type',
      'handler' => 'views_handler_filter_string',
      'label' => 'Type',
      
    ),
  );
  
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('items_per_page', 10);
  $handler->override_option('fields', array(
    'name' => array(
      'label' => 'Name',
      'exclude' => 0,
      'id' => 'name',
      'table' => 'bookings_records',
      'field' => 'name',
      'relationship' => 'none',
    ),
    'start' => array(
      'label' => 'Starts',
      'date_format' => 'large',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'start',
      'table' => 'bookings_schedules',
      'field' => 'start',
      'relationship' => 'none',
    ),
    'end' => array(
      'label' => 'Ends',
      'date_format' => 'large',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'end',
      'table' => 'bookings_schedules',
      'field' => 'end',
      'relationship' => 'none',
    ),
    'type' => array(
      'label' => 'Type',
      'exclude' => 0,
      'id' => 'type',
      'table' => 'bookings_schedules',
      'field' => 'type',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'disabled' => array(
      'operator' => '=',
      'value' => 0,
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'disabled',
      'table' => 'bookings_resources',
      'field' => 'disabled',
      'relationship' => 'none',
    ),
    'resource_id' => array(
      'operator' => 'in',
      'value' => array(),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'resource_id_op',
        'identifier' => 'resource_id',
        'label' => 'Resource',
        'optional' => 0,
        'single' => 1,
        'remember' => 0,
        'reduce' => 0,
      ),
      'id' => 'resource_id',
      'table' => 'bookings_schedules',
      'field' => 'resource_id',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
    'role' => array(),
    'perm' => '',
  ));

  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 1,
    'order' => 'asc',
    'columns' => array(
      'end' => 'end',
      'start' => 'start',
      'name' => 'name',
      'resource_name' => 'resource_name',
      'type' => 'type',
    ),
    'info' => array(
      'end' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'start' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'name' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'resource_name' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'type' => array(
        'sortable' => 1,
        'separator' => '',
      ),
    ),
    'default' => 'start',
  ));
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('path', 'publicbookings/schedules');
  $handler->override_option('menu', array(
    'type' => 'none',
    'title' => '',
    'weight' => 0,
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'weight' => 0,
  ));

  return $view;
}

function publicbookings_view_schedules_admin() {
  $view = publicbookings_default_view_object();
  $view->name = 'publicbookings_view_schedules';
  $view->base_table = 'bookings_schedules';
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('use_pager', TRUE);
  $handler->override_option('items_per_page', '10');
  $handler->override_option('fields', array(
    'name' => array(
      'label' => 'Name',
      'exclude' => 0,
      'id' => 'name',
      'table' => 'bookings_records',
      'field' => 'name',
      'relationship' => 'none',
    ),
    'start' => array(
      'label' => 'Starts',
      'date_format' => 'large',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'start',
      'table' => 'bookings_schedules',
      'field' => 'start',
      'relationship' => 'none',
    ),
    'end' => array(
      'label' => 'Ends',
      'date_format' => 'large',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'end',
      'table' => 'bookings_schedules',
      'field' => 'end',
      'relationship' => 'none',
    ),
    'resource_name' => array(
      'label' => 'Resource',
      'exclude' => 0,
      'id' => 'resource_name',
      'table' => 'bookings_resources',
      'field' => 'name',
      'relationship' => 'none',
    ),
    'edit_booking' => array(
      'label' => '',
      'id' => 'edit_booking',
      'table' => 'bookings_records',
      'field' => 'edit_booking',
    ),
    'delete_booking' => array(
      'label' => '',
      'id' => 'delete_booking',
      'table' => 'bookings_records',
      'field' => 'delete_booking',
    ),
  ));
  $handler->override_option('filters', array(
    'type' => array(
      'operator' => 'in',
      'value' => array(1),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'type_op',
        'identifier' => 'type',
        'label' => 'Type',
        'optional' => 0,
        'single' => 1,
        'remember' => 0,
        'reduce' => 0,
      ),
      'id' => 'type',
      'table' => 'bookings_records',
      'field' => 'type',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'type' => array(
      'default_action' => 'ignore',
      'style_plugin' => 'not found',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '',
      'default_argument_type' => 'fixed',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'not found',
      'break_phrase' => 0,
      'not' => 0,
      'id' => 'type',
      'table' => 'bookings_records',
      'field' => 'status',
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_user' => 0,
      'default_argument_fixed' => '',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'blog' => 0,
        'poll' => 0,
        'feedapi_node' => 0,
        'page' => 0,
        'story' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(
        '1' => 0,
      ),
      'validate_argument_type' => 'tid',
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
    'role' => array(),
    'perm' => '',
  ));
  $handler->override_option('empty', t('None.'));

  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 1,
    'order' => 'asc',
    'columns' => array(
      'end' => 'end',
      'start' => 'start',
      'name' => 'name',
      'resource_name' => 'resource_name',
      'type' => 'type',
    ),
    'info' => array(
      'end' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'start' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'name' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'resource_name' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'type' => array(
        'sortable' => 1,
        'separator' => '',
      ),
    ),
    'default' => 'start',
  ));

  return $view;
}

function publicbookings_view_current() {
  $view = publicbookings_default_view_object();
  $view->name = 'publicbookings_view_current';
  $view->base_table = 'bookings_schedules';
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'end' => array(
      'label' => 'Ends',
      'date_format' => 'large',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'end',
      'table' => 'bookings_schedules',
      'field' => 'end',
      'relationship' => 'none',
    ),
    'start' => array(
      'label' => 'Starts',
      'date_format' => 'large',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'start',
      'table' => 'bookings_schedules',
      'field' => 'start',
      'relationship' => 'none',
    ),
    'name' => array(
      'label' => 'Name',
      'exclude' => 0,
      'id' => 'name',
      'table' => 'bookings_records',
      'field' => 'name',
      'relationship' => 'none',
    ),
    'resource_name' => array(
      'label' => 'Resource',
      'exclude' => 0,
      'id' => 'resource_name',
      'table' => 'bookings_resources',
      'field' => 'name',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('sorts', array(
    'type' => array(
      'id' => 'type',
      'table' => 'bookings_records',
      'field' => 'type',
    ),
  ));
  $handler->override_option('filters', array(
    'type' => array(
      'operator' => 'in',
      'value' => array(
        '1' => 1,
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'bookings_schedules',
      'field' => 'type',
      'relationship' => 'none',
    ),
    'end' => array(
      'operator' => 'between',
      'value' => array(
        'type' => 'offset',
        'value' => '',
        'min' => '-1 hour',
        'max' => '+1 hour',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'end',
      'table' => 'bookings_schedules',
      'field' => 'end',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
    'role' => array(),
    'perm' => '',
  ));
  $handler->override_option('empty', 'Nothing to display.');
  $handler->override_option('empty_format', '1');
  $handler->override_option('style_plugin', 'table');
  return $view;
}

function publicbookings_view_availability_admin() {
  $view = publicbookings_default_view_object();
  $view->name = 'publicbookings_view_availability_admin';
  $view->base_table = 'bookings_records';
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'type' => array(
      'label' => 'Type',
      'exclude' => 0,
      'id' => 'type',
      'table' => 'bookings_records',
      'field' => 'type',
      'relationship' => 'none',
    ),
    'name' => array(
      'label' => 'Name',
      'exclude' => 0,
      'id' => 'name',
      'table' => 'bookings_records',
      'field' => 'name',
      'relationship' => 'none',
    ),
    'start' => array(
      'label' => 'Start',
      'date_format' => 'small',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'start',
      'table' => 'bookings_records',
      'field' => 'start',
      'relationship' => 'none',
    ),
    'end' => array(
      'label' => 'End',
      'date_format' => 'small',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'end',
      'table' => 'bookings_records',
      'field' => 'end',
      'relationship' => 'none',
    ),
    'rrule' => array(
      'label' => 'Repeat',
      'exclude' => 0,
      'id' => 'rrule',
      'table' => 'bookings_records',
      'field' => 'rrule',
      'relationship' => 'none',
    ),
    'edit_availability' => array(
      'label' => '',
      'exclude' => 0,
      'id' => 'edit_availability',
      'table' => 'bookings_records',
      'field' => 'edit_availability',
      'relationship' => 'none',
    ),
    'delete_availability' => array(
      'label' => '',
      'exclude' => 0,
      'id' => 'delete_availability',
      'table' => 'bookings_records',
      'field' => 'delete_availability',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'type' => array(
      'operator' => 'in',
      'value' => array(
        '2' => 2,
        '3' => 3,
        '4' => 4,
        '5' => 5,
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'case' => 0,
      'id' => 'type',
      'table' => 'bookings_records',
      'field' => 'type',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'resource_id' => array(
      'default_action' => 'empty',
      'style_plugin' => 'not found',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '',
      'default_argument_type' => 'fixed',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'not found',
      'break_phrase' => 0,
      'not' => 0,
      'id' => 'resource_id',
      'table' => 'bookings_records',
      'field' => 'resource_id',
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_user' => 0,
      'default_argument_fixed' => '',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'blog' => 0,
        'poll' => 0,
        'feedapi_node' => 0,
        'page' => 0,
        'story' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(
        '1' => 0,
      ),
      'validate_argument_type' => 'tid',
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
    'role' => array(),
    'perm' => '',
  ));
  $handler->override_option('empty', t('No additional availability information defined for this resource.'));
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'delete_availability' => 'delete_availability',
      'edit_availability' => 'edit_availability',
      'end' => 'end',
      'start' => 'start',
      'name' => 'name',
    ),
    'info' => array(
      'delete_availability' => array(
        'separator' => '',
      ),
      'edit_availability' => array(
        'separator' => '',
      ),
      'type' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'end' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'start' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'name' => array(
        'sortable' => 1,
        'separator' => '',
      ),
    ),
    'default' => 'name',
  ));
  return $view;
}

function publicbookings_view_resources_admin() {
  $view = publicbookings_default_view_object();
  $view->name = 'publicbookings_view_resources_admin';
  $view->base_table = 'bookings_resources';
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('title', t('Resources'));
  $handler->override_option('fields', array(
    'name' => array(
      'label' => 'Name',
      'exclude' => 0,
      'id' => 'name',
      'table' => 'bookings_resources',
      'field' => 'name',
      'relationship' => 'none',
    ),
    'location' => array(
      'label' => 'Location',
      'exclude' => 0,
      'id' => 'location',
      'table' => 'bookings_resources',
      'field' => 'location',
      'relationship' => 'none',
    ),
    'disabled' => array(
      'label' => 'Disabled',
      'exclude' => 0,
      'id' => 'disabled',
      'table' => 'bookings_resources',
      'field' => 'disabled',
      'relationship' => 'none',
    ),
    'default_availability' => array(
      'label' => 'Default availability',
      'exclude' => 0,
      'id' => 'default_availability',
      'table' => 'bookings_resources',
      'field' => 'default_availability',
      'relationship' => 'none',
    ),
    'edit_resource' => array(
      'label' => '',
      'exclude' => 0,
      'id' => 'edit_resource',
      'table' => 'bookings_resources',
      'field' => 'edit_resource',
      'relationship' => 'none',
    ),
    'delete_resource' => array(
      'label' => '',
      'exclude' => 0,
      'id' => 'delete_resource',
      'table' => 'bookings_resources',
      'field' => 'delete_resource',
      'relationship' => 'none',
    ),
    'list_availability' => array(
      'label' => '',
      'exclude' => 0,
      'id' => 'list_availability',
      'table' => 'bookings_resources',
      'field' => 'list_availability',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
    'role' => array(),
    'perm' => '',
  ));
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'name' => 'name',
      'location' => 'location',
      'description' => 'description',
      'disabled' => 'disabled',
      'default_availability' => 'default_availability',
      'edit_resource' => 'edit_resource',
      'delete_resource' => 'delete_resource',
      'list_availability' => 'list_availability',
    ),
    'info' => array(
      'name' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'location' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'description' => array(
        'separator' => '',
      ),
      'disabled' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'default_availability' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'edit_resource' => array(
        'separator' => '',
      ),
      'delete_resource' => array(
        'separator' => '',
      ),
      'list_availability' => array(
        'separator' => '',
      ),
    ),
    'default' => 'name',
  ));
  return $view;
}

function publicbookings_view_currently_happening() {
  $view = publicbookings_default_view_object();
  $view->name = 'publicbookings_view_currently_happening';
  $view->base_table = 'bookings_schedules';
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'name' => array(
      'label' => 'Name',
      'exclude' => 0,
      'id' => 'name',
      'table' => 'bookings_records',
      'field' => 'name',
      'relationship' => 'none',
    ),
    'name_1' => array(
      'label' => 'Resource',
      'exclude' => 0,
      'id' => 'name_1',
      'table' => 'bookings_resources',
      'field' => 'name',
      'relationship' => 'none',
    ),
    'start' => array(
      'label' => 'Start',
      'date_format' => 'small',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'start',
      'table' => 'bookings_schedules',
      'field' => 'start',
      'relationship' => 'none',
    ),
    'end' => array(
      'label' => 'End',
      'date_format' => 'small',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'end',
      'table' => 'bookings_schedules',
      'field' => 'end',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'start' => array(
      'operator' => '<=',
      'value' => array(
        'type' => 'date',
        'value' => date(DATE_FORMAT_DATETIME),
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'start',
      'table' => 'bookings_schedules',
      'field' => 'start',
      'relationship' => 'none',
    ),
    'end' => array(
      'operator' => '>=',
      'value' => array(
        'type' => 'date',
        'value' => date(DATE_FORMAT_DATETIME),
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'end',
      'table' => 'bookings_schedules',
      'field' => 'end',
      'relationship' => 'none',
    ),
    'type' => array(
      'operator' => 'in',
      'value' => array(
        '1' => 1,
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'bookings_records',
      'field' => 'type',
      'relationship' => 'none',
    ),
    'status' => array(
      'operator' => 'in',
      'value' => array(
        '2' => 2,
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'case' => 1,
      'id' => 'status',
      'table' => 'bookings_records',
      'field' => 'status',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
    'role' => array(),
    'perm' => '',
  ));
  $handler->override_option('empty', 'None.');
  $handler->override_option('empty_format', '1');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'end' => 'end',
      'start' => 'start',
      'name' => 'name',
    ),
    'info' => array(
      'end' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'start' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'name' => array(
        'sortable' => 0,
        'separator' => '',
      ),
    ),
    'default' => 'start',
  ));
  return $view;
}

function publicbookings_view_upcoming() {
  $view = publicbookings_default_view_object();
  $view->name = 'publicbookings_view_upcoming';
  $view->base_table = 'bookings_schedules';
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'name' => array(
      'label' => 'Name',
      'exclude' => 0,
      'id' => 'name',
      'table' => 'bookings_records',
      'field' => 'name',
      'relationship' => 'none',
    ),
    'name_1' => array(
      'label' => 'Resource',
      'exclude' => 0,
      'id' => 'name_1',
      'table' => 'bookings_resources',
      'field' => 'name',
      'relationship' => 'none',
    ),
    'start' => array(
      'label' => 'Start',
      'date_format' => 'small',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'start',
      'table' => 'bookings_schedules',
      'field' => 'start',
      'relationship' => 'none',
    ),
    'end' => array(
      'label' => 'End',
      'date_format' => 'small',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'end',
      'table' => 'bookings_schedules',
      'field' => 'end',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'start' => array(
      'operator' => '>=',
      'value' => array(
        'type' => 'date',
        'value' => date(DATE_FORMAT_DATETIME),
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'start',
      'table' => 'bookings_schedules',
      'field' => 'start',
      'relationship' => 'none',
    ),
    'end' => array(
      'operator' => '>=',
      'value' => array(
        'type' => 'offset',
        'value' => '+12 hours',
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'end',
      'table' => 'bookings_schedules',
      'field' => 'end',
      'relationship' => 'none',
    ),
    'type' => array(
      'operator' => 'in',
      'value' => array(
        '1' => 1,
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'bookings_records',
      'field' => 'type',
      'relationship' => 'none',
    ),
    'status' => array(
      'operator' => 'in',
      'value' => array(
        '2' => 2,
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'case' => 1,
      'id' => 'status',
      'table' => 'bookings_records',
      'field' => 'status',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
    'role' => array(),
    'perm' => '',
  ));
  $handler->override_option('empty', 'None.');
  $handler->override_option('empty_format', '1');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'end' => 'end',
      'start' => 'start',
      'name' => 'name',
    ),
    'info' => array(
      'end' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'start' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'name' => array(
        'sortable' => 0,
        'separator' => '',
      ),
    ),
    'default' => 'start',
  ));
  return $view;
}