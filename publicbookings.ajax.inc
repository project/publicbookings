<?php
/**
 * @file
 * Public Bookings AJAX conflict-checking functions.
 */

/**
 * Process the AJAX-submitted form fields into an array of the same format
 * accepted by bookingsapi_conflict_check(). Takes values directly from $_POST.
 *
 * @return array
 * @see ajax.js
 */
function publicbookings_ajax_post_prepare() {
  $params = array();
  // cast all POST fields to ints, to sanitize.
  if (is_numeric($_POST['resource_id'])) {
    $params['resource_id'] = (int) $_POST['resource_id'];
    $params['record_id'] = (int) $_POST['record_id'];
  } else {
    // invalid resource ID - quit
    return;
  }
  // currently making the assumption that the fields are in date_select format.
  if (is_array($_POST['start']) && is_array($_POST['end'])) {
    foreach ($_POST['start'] as &$v) {
      $v = (int) $v;
    }
    foreach ($_POST['end'] as &$v) {
      $v = (int) $v;
    }
  } else {
    // invalid start or end - quit
    return;
  }

  // convert start and end time fields to DATETIME.
  $params['start'] = date_convert($_POST['start'], DATE_ARRAY, DATE_DATETIME);
  $params['end'] = date_convert($_POST['end'], DATE_ARRAY, DATE_DATETIME);

  // parse form
  $until = $_POST['UNTIL']['datetime'];
  $exdate0 = $_POST['EXDATE'][0]['datetime'];
  if (empty($until['year']) || empty($until['month']) || empty($until['day'])) {
      // invalid RRULE because there's no valid UNTIL
  } else {
    include_once('./'. drupal_get_path('module', 'date_repeat') .'/date_repeat_form.inc');
    include_once('./'. drupal_get_path('module', 'date_api') .'/date_api_ical.inc');

    // date_repeat_rrule_validate runs this before building RRULE, so we do this too.
    $processed = date_repeat_merge($_POST);

    // if first EXDATE is invalid, unset the entire EXDATE array to prevent errors
    if($processed['EXDATE'][0]['datetime'] == '0000-00-00 00:00:00') unset($processed['EXDATE']);

    $params['rrule'] = date_api_ical_build_rrule( $processed );
    $params['rrule_until'] = bookingsapi_rrule_until_as_dt($params['rrule']);
  }

  return $params;
}

/**
 * Checks for conflicts and potential conflicts of records, with value supplied from $_POST.
 *
 * @return string
 *  JSON HTML
 * @see ajax.js
 */
function publicbookings_ajax_query() {
  $params = publicbookings_ajax_post_prepare();
  if($params == NULL) return;
  publicbookings_conflict_check($params);
  print drupal_to_js($params['html_all_conflicts']);
  exit();
}